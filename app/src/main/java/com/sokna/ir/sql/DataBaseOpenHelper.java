package com.sokna.ir.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class DataBaseOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseOpenHelper";

    private static final String DATABASE_NAME="db_7learn";
    private static final int DATABASE_VERSION=1;

    private static final String POST_TABLE_NAME="tbl_posts";


    public static final String COL_ID="col_id";
    public static final String COL_TITLE="col_title";
    public static final String COL_CONTENT="col_content";
    public static final String COL_POST_IMAGE_URL="col_post_image_url";
    public static final String COL_IS_VISITED="col_is_visited";
    public static final String COL_DATE="col_date";


    private static final String SQL_COMMAND_CREATE_POST_TABLE="CREATE TABLE IF NOT EXISTS "+POST_TABLE_NAME+"("+
            COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            COL_TITLE+" TEXT,"+
            COL_CONTENT+" TEXT, "+
            COL_POST_IMAGE_URL+" TEXT, "+
            COL_IS_VISITED+" INTEGER DEFAULT 0, "+
            COL_DATE+" TEXT);";



    Context context;
    public DataBaseOpenHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
        /// tozih khat bala name = name database , version = versio data base k bad az taghir methode onUpgrade farakhani mishe

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(SQL_COMMAND_CREATE_POST_TABLE);
        }catch (SQLException e){
            Log.e(TAG, "onCreate: "+e.toString() );
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }

    ///shayad bkhaym yek done post dakhel database add konim
    public boolean addPost(Post post){
        ContentValues cv=new ContentValues();
        //// sakhtar key value dare

        //// tartib moheme
        cv.put(COL_ID,post.getId());
        cv.put(COL_TITLE,post.getTitle());
        cv.put(COL_CONTENT,post.getContent());
        cv.put(COL_POST_IMAGE_URL,post.getPostImageUrl());
        cv.put(COL_IS_VISITED,post.getIsVisited());
        cv.put(COL_DATE,post.getDate());


        ////// getWritableDatabase() zamani k bkhayim data vared konim
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        long isInserted = sqLiteDatabase.insert(POST_TABLE_NAME,null,cv);

        Log.i(TAG, "addPost: "+isInserted);

        if (isInserted>0){
            return true;
        }else{
            return false;
        }
    }


    ////asli hamine
    ///shayad bkhaym listi az  post ha ro  dakhel database add konim
    public void addPosts(List<Post> posts){

        //// har post be table ezafe kon
        for (int i = 0; i < posts.size(); i++) {

            //// age post vojud nadasht post add kon
            if (!checkPostExists(posts.get(i).getId())) {
                addPost(posts.get(i));
            }
        }
    }


    //// list post ha ro bar migardune
    public List<Post> getPosts(){
        List<Post> posts=new ArrayList<>();

        SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();

        //// ba in query k neveshtim hame data o barmigardune * yani hame
        Cursor cursor=sqLiteDatabase.rawQuery("SELECT * FROM "+POST_TABLE_NAME,null);

        // cursor alan ro avalin khunast chon goftim move to first
        cursor.moveToFirst();

        /// age cursor meghadar dasht
        if (cursor.getCount()>0){

            /// ta zamani k cursor be akharin khune narisde in kara o anjam bede
            while (!cursor.isAfterLast()){
                Post post=new Post();
                /// in adad dakhel parantez yani chadomin setun hast k alan id seton 0 hast chon az 0 shoru mishe
                post.setId(cursor.getInt(0));
                post.setTitle(cursor.getString(1));
                post.setContent(cursor.getString(2));
                post.setPostImageUrl(cursor.getString(3));
                post.setIsVisited(cursor.getInt(4));
                post.setDate(cursor.getString(5));
                posts.add(post);

                cursor.moveToNext();
            }
        }

        //// bad az tatmum shodan karemun bayad bbndim chon ram ziad migire
        cursor.close();
        sqLiteDatabase.close();
        return posts;
    }


    ////vaghti kar bar post o nega krd miad yek mikone


    public void setPostIsVisited(int postId,int isVisited){
        SQLiteDatabase sqliteDatabase=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        /// taghire setune isvisited dar vaghe darim in seton update mikonim
        contentValues.put(COL_IS_VISITED,isVisited);
        int rowAffected=sqliteDatabase.update(POST_TABLE_NAME,contentValues,COL_ID+" = ?",new String[]{String.valueOf(postId)});

        Log.i(TAG, "setPostIsVisited: rowAffected=> "+rowAffected);
        sqliteDatabase.close();
    }


    ///// age oun id vjud dasht dg add nakon
    private boolean checkPostExists(int postId){
        SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();
        Cursor cursor=sqLiteDatabase.rawQuery("SELECT * FROM "
                +POST_TABLE_NAME
                +" WHERE "
                +COL_ID
                +" = ?",new String[]{String.valueOf(postId)});
        return cursor.moveToFirst();
    }


    /// baraye pak kardn az data base
    private void deletePost(int postId){
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        sqLiteDatabase.delete(POST_TABLE_NAME,COL_ID+" = ?",new String[]{String.valueOf(postId)});
    }

}
