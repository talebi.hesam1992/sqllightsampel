package com.sokna.ir.sql;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Post> posts;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getPostsFromDataBase();


        int id = 2;/// dar vaghe chon inja serveri ndrm darim dasti midam vali tu poroje asli in id hamon id posti hast k mikham update beshe va moheme


        setPostVisited(id);


        DataBaseOpenHelper openHelper = new DataBaseOpenHelper(this);

        // inja bayad ye listi besh pas bedim age be server vasl budim hamun list o besh pas midim List<Post> post

        openHelper.addPosts(posts);


        recyclerView = findViewById(R.id.recycler_view);
        MyAdapter newsAdapter = new MyAdapter(this, DataFakeGenerator.getData(this));
        //recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        //recyclerView.setLayoutManager(new GridLayoutManager(this,2,LinearLayoutManager.VERTICAL,false));

        recyclerView.setAdapter(newsAdapter);


    }



    /// ba in function k tarif kardm etelaate data base mirizm tu recycel bara zamani khube k hanuz be net vasl nist app
    private void getPostsFromDataBase() {

        DataBaseOpenHelper openHelper = new DataBaseOpenHelper(this);
        List<Post> posts= openHelper.getPosts();

        MyAdapter myAdapter= new MyAdapter(this,posts);
        recyclerView.setAdapter(myAdapter);
    }


    /// yek satr az data base darim update mikonim
    private void setPostVisited(int postId){
        DataBaseOpenHelper databaseOpenHelper=new DataBaseOpenHelper(this);

        databaseOpenHelper.setPostIsVisited(postId,1);
    }


}
