package com.sokna.ir.sql;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by hesam talebi on 7/22/2017.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.NewsViewHolder> {


    private Context context;
    private List<Post> posts;

    public MyAdapter(Context context, List<Post> posts){
        this.context = context;
        this.posts = posts;
    }



    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.layout_news,parent,false);
        return new NewsViewHolder(view);
    }



    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        Post post=posts.get(position);
        holder.newsImage.setImageDrawable(post.getPostImage());
        holder.title.setText(post.getTitle());
        holder.content.setText(post.getContent());
        holder.date.setText(post.getDate());
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }





    public class NewsViewHolder extends RecyclerView.ViewHolder{
        private ImageView newsImage;
        private TextView title;
        private TextView content;
        private TextView date;

        public NewsViewHolder(View itemView) {
            super(itemView);
            newsImage=itemView.findViewById(R.id.item_image);
            title=itemView.findViewById(R.id.news_title);
            content=itemView.findViewById(R.id.news_content);
            date=itemView.findViewById(R.id.news_date);
        }
    }
}